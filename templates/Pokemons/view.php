<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pokemon $pokemon
 * @var \App\Model\Entity\Stat $stat
 * @var \App\Model\Entity\PokemonStat $pokemonstat
 */
?>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <div class="pokemons view content">
            <h1><?= h($pokemon->name) ?></h1>
                <figcaption class="card__caption">
                    <h3 class="card__type <?= $pokemon->first_type ?>">
                        <?= $pokemon->first_type ?>
                    </h3>
                    <h3 class="card__second_type <?= $pokemon->second_type ?>">
                        <?= $pokemon->second_type ?>
                    </h3>
        </div>
    </header>
    <main>
        <div class="rows">
            <div class="column" style="background-color:#aaa;">
                <?= $this->Html->image($pokemon->main_sprite); ?>
            </div>
            <div class="column" style="background-color:#bbb;">
                <div class="pokemons view content">
                    <table class="card__stats">
                        <tbody>
                            <tr>
                                <th>hp</th>
                                <td><?= $pokemon->first_stat?></td>
                            </tr>
                            <tr>
                                <th>Defense</th>
                                <td><?= $pokemon->third_stat?></td>
                            </tr>
                            <tr>
                                <th>Attack</th>
                                <td><?= $pokemon->second_stat?></td>
                            </tr>
                            <tr>
                                <th>Special Attack</th>
                                <td><?= $pokemon->four_stat ?></td>
                            </tr>
                            <tr>
                                <th>Special Defense</th>
                                <td><?= $pokemon->five_stat ?></td>
                            </tr>
                            <tr>
                                <th>Speed</th>
                                <td><?= $pokemon->six_stat ?></td>
                            </tr>
                        </tbody>
                    </table>  
                </div>
            </div>
        </div>
</main>
    <footer>
        <div class="pokemons view content">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="card__image-container poke-sprite-<?= $pokemon->pokedex_number ?>" id="main-poke-sprites">
                            <?= $this->Html->image($pokemon->main_sprite); ?>
                        </div>
                    </div>

                    <div class="item">
                        <div class="card__image-container poke-sprite-<?= $pokemon->pokedex_number ?>" id="main-poke-sprites">
                            <?php if (!$pokemon->default_back_sprite_url) : ?>
                                <?= $this->Html->image($pokemon->main_sprite); ?>
                            <?php endif ?>
                            <?php if ($pokemon->default_back_sprite_url) : ?>
                                <?= $this->Html->image($pokemon->default_back_sprite_url); ?>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="item">
                        <div class="card__image-container poke-sprite-<?= $pokemon->pokedex_number ?>" id="main-poke-sprites">
                            <?php if (!$pokemon->Shiny_front_sprite_url) : ?>
                                <?= $this->Html->image($pokemon->main_sprite); ?>
                            <?php endif ?>
                            <?php if ($pokemon->Shiny_front_sprite_url) : ?>
                                <?= $this->Html->image($pokemon->Shiny_front_sprite_url); ?>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="item">
                        <div class="card__image-container poke-sprite-<?= $pokemon->pokedex_number ?>" id="main-poke-sprites">
                            <?php if (!$pokemon->Shiny_back_sprite_url) : ?>
                                <?= $this->Html->image($pokemon->main_sprite); ?>
                            <?php endif ?>
                            <?php if ($pokemon->Shiny_back_sprite_url) : ?>
                                <?= $this->Html->image($pokemon->Shiny_back_sprite_url); ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </footer>
</body>

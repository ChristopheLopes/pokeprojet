<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pokemon $pokemon
 * @var \App\Model\Entity\Stat $stat
 * @var \App\Model\Entity\PokemonStat $pokemonstat
 * @var \App\Model\Table\PokemonsTable $poketable
 * 
 */
use \App\Model\Table\PokemonsTable;

 $poketable = new PokemonsTable();
 $index = 1;
?>
<table>
<h3>Nombre de Pokémon de type fée (1,3,7 Gen): </h3>
<tr>
    
    <td> <?= $poketable->FAIRY()->fee?>  </td>
</tr>
</table>
<br>

<h3>Moyenne du poids des Pokémons de 4Gen : </h3>
<table>
<tr>
    
    <td><?= ($poketable->POIDS()->poids)/10 ?> kg</td>
</tr>
</table>
<br>


<h3>10 Premier Pokémon avec la Vitesse la plus haute : </h3>
<table>
<tr>
<th>Position</th>
<th>Pokémon</th>
<th>Vitesse</th>
</tr>

    <?php foreach($poketable->STATS() as $value):?>
<tr>    
<td>
<?= $index++ ?>. 
</td>
<td>
<?= $value->name?>
</td>
<td>
<?= $value->value?>
</td>
</tr>
    <?php endforeach ?>


</table>
